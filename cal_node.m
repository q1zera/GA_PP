%% 计算路径平滑度函数
%jubobolv369
function [node] = cal_node(pop, x)
[n, ~] = size(pop);
node = zeros(1, n);
%循环计算每一条路径的平滑度
for i = 1 : n
    sum_node=0;
    single_pop = pop{i, 1};
    [~, m] = size(single_pop);
    %路径有m个栅格，需要计算m-1次
    for j = 1 : m - 2
        % 点i所在列（从左到右编号1.2.3...）
        x_now = mod(single_pop(1, j), x) + 1;
        % 点i所在行（从上到下编号行1.2.3...）
        y_now = fix(single_pop(1, j) / x) + 1;
        % 点i+1所在列、行
        x_next = mod(single_pop(1, j + 1), x) + 1;
        y_next = fix(single_pop(1, j + 1) / x) + 1;
        dis = sqrt((x_next-x_now)^2+(y_next-y_now)^2);
        if dis>=sqrt(2)
            sum_node=sum_node+1;
        end
    end
    node(1,i)=sum_node;
end
end