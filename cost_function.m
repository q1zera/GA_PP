function [fit_value,path_value,node_value] = cost_function(pop,x,r,a,b)
% 计算路径长度
path_value = cal_path_value(pop, x)
% 计算路径拐点数量
node_value = cal_node(pop, x)
L=path_value;
T=node_value;
fit_value = r./(a*L+b*T);%a .* path_value .^ -1 + b .* path_smooth .^ -1;
end

