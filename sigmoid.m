function [pc,pm] = sigmoid(pc_max,pc_min,pm_max,pm_min,fit_value,new_pop2)
total_fit = sum(fit_value);
[pop, ~] = size(new_pop2);
p_fit_value = total_fit/pop
% pc=0.8;
% pm=0.2;
index=p_fit_value-fit_value
pc = pc_min + (pc_max-pc_min).*(1./(1+exp(-1*index)));
pm = pm_min + (pm_max-pm_min).*(1./(1+exp(-1*index)));
end

